import React, { Component } from "react";
import { StyleSheet, View, Text, PanResponder, Animated } from "react-native";
import Draggable from 'react-native-draggable';


export default class App extends Component {
  constructor(props) {
    super(props);

    console.disableYellowBox = true;

    this.state = {
      update: false,
      numbers: [1],
    };
    
  }
  componentDidMount(){
    //setInterval(()=>this.updateElement(), 3000);
  }
  updateElement(){
    a = null;
    a = this.state.numbers;
    a.push(2);
    this.setState({numbers: a});
    console.log('update');
  }

  test(a){
    console.log(a);
  }

  render() {
    return (
      <View >
        <Draggable x={75} y={100} renderSize={56} renderColor='black' renderText='A' isCircle onDragRelease={()=>this.test(1)}/> 
        <Draggable x={200} y={300} renderColor='red' renderText='B'/>

    </View>
    );
  }
}

let CIRCLE_RADIUS = 30;
const styles = StyleSheet.create({
  mainContainer: {
    position: "absolute",
  },
  ballContainer: {
    height:200
  },
  circle: {
    backgroundColor: "skyblue",
    width: CIRCLE_RADIUS * 2,
    height: CIRCLE_RADIUS * 2,
    borderRadius: CIRCLE_RADIUS
  },
  row: {
    flexDirection: "row"
  },  
  dropZone: {
    height: 200,
    backgroundColor: "#00334d"
  },
  text: {
    marginTop: 25,
    marginLeft: 5,
    marginRight: 5,
    textAlign: "center",
    color: "#fff",
    fontSize: 25,
    fontWeight: "bold"
  }
});